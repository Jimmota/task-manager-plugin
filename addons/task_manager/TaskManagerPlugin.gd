tool
extends EditorPlugin

const SectionScn = preload("res://addons/task_manager/scenes/Section.tscn")
const TaskManagerScn = preload("res://addons/task_manager/scenes/TaskManager.tscn")
const TaskScn = preload("res://addons/task_manager/scenes/Task.tscn")

var TaskManager : VBoxContainer
var editor_settings : EditorSettings

func _enter_tree() -> void:
	editor_settings = get_editor_interface().get_editor_settings()
	TaskManager = TaskManagerScn.instance()
	get_editor_interface().get_editor_viewport().add_child(TaskManager)
	make_visible(false)
	
func _exit_tree() -> void:
	TaskManager.queue_free()
	
func _ready() -> void:
	# Loading previously saved sections and tasks
	var sections : Array = editor_settings.get_project_metadata("task_manager", "sections")
	for s in sections:
		var NewSection = SectionScn.instance()
		TaskManager.SectionContainer.add_child(NewSection)
		NewSection.TitleLabel.text = s.title

		for t in s.children:
			var NewTask = TaskScn.instance()
			NewTask.connect("pressed", NewSection, "_on_task_pressed")
			NewTask.connect("tree_exited", NewSection, "_on_task_tree_exited")
			NewTask.text = t.title
			NewTask.priority = t.priority
			NewTask.pressed = t.pressed
			NewSection.TaskContainer.add_child(NewTask)
			NewTask.CommentTextEdit.text = t.comments
			NewTask.update_comment_controls()

		NewSection.update_progress()
		NewSection.update_title_controls()

	
func has_main_screen() -> bool:
   return true

func make_visible(visible : bool) -> void:
   if visible:
      TaskManager.show()
   else:
      TaskManager.hide()
	
func get_plugin_name() -> String:
   return "Task Manager"

func save_external_data() -> void:
	# Saving current sections and tasks
	var sections : Array = []
	for s in TaskManager.SectionContainer.get_children():
		sections.append({
			"title" : s.TitleLabel.text,
			"children" : []
			})
		for t in s.TaskContainer.get_children():
			sections[sections.size() - 1].children.append({
				"title" : t.text,
				"priority" : t.priority,
				"pressed" : t.pressed,
				"comments" : t.CommentTextEdit.text
				})
	
	editor_settings.set_project_metadata("task_manager", "sections", sections)
